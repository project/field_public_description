<?php

/**
 * Implements hook_form_alter().
 */
function field_public_description_form_alter(&$form, &$form_state, $form_id) {
  $children = array_intersect_key($form, array_flip(element_children($form)));
  foreach ($children as $key => $item) {
    if (is_array($item) && isset($item['#language']) && isset($item[$item['#language']])
      && isset($form['#entity_type']) && isset($form['#bundle'])
      && ($instance = field_info_instance($form['#entity_type'], $key, $form['#bundle']))
    ) {
      if (!isset($instance['widget']['settings']['field_public_description'])) {
        continue;
      }
      $theme_option = array(
        'description at top' => $instance['widget']['settings']['field_public_description'],
      );
      // Put comments above the label for field forms of type 'container'
      // that are specifically configured.
      if (isset($item['#type']) && $item['#type'] == 'container' && isset($item[$item['#language']][0])) {
        // For reasons best known to the lunatics who designed the Forms API,
        // $form[$key][$item['#language']][0]['#theme_options'] has to be set to get
        // this working for textarea fields, and
        // form[$key][$item['#language']][0]['value']['#theme_options'] has to be set
        // to get this working for one-line text fields.
        $form[$key][$item['#language']][0]['#theme_options'] = $form[$key][$item['#language']][0]['value']['#theme_options'] = $theme_option;
      }
      // Move comments to the top for other field forms that
      // are specifically configured.
      else {
        $form[$key][$item['#language']]['#theme_options'] = $form[$key][$item['#language']]['value']['#theme_options'] = $theme_option;
      }
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function field_public_description_form_field_ui_field_edit_form_alter(&$form, &$form_state, $form_id) {
  // Add settings for file upload widgets.
  $form['instance']['widget']['settings']['field_public_description'] = array(
    '#type' => 'textarea',
    '#rows' => 2,
    '#title' => t('Public description (for end users)'),
    '#default_value' => isset($form['#instance']['widget']['settings']['field_public_description']) ? $form['#instance']['widget']['settings']['field_public_description'] : '',
    '#description' => t('A text explaining what means this field information. Is oriented to end users, not content administrators.'),
  );

  // Ask the user where to display the public description.
  $form['instance']['widget']['settings']['field_public_description_position'] = array(
    '#type' => 'select',
    '#title' => t('Public description position'),
    '#options' => array(
      'above' => t('Above the field value'),
      'below' => t('Below the field value'),
    ),
    '#default_value' => isset($form['#instance']['widget']['settings']['field_public_description_position']) ? $form['#instance']['widget']['settings']['field_public_description_position'] : 'above',
  );
}

function field_public_description_preprocess_field(&$vars) {
  $instance = field_info_instance($vars['element']['#entity_type'], $vars['element']['#field_name'], $vars['element']['#bundle']);
  if (isset($instance['widget'])
    && isset($instance['widget']['settings']['field_public_description'])
  ) {

    if ($instance['widget']['settings']['field_public_description_position'] === 'above') {
      array_unshift($vars['items'], array(
        '#markup' => '<div class="public-description">' . t($instance['widget']['settings']['field_public_description']) . '</div>'
      ));
    }
    else {
      array_push($vars['items'], array(
        '#markup' => '<div class="public-description">' . t($instance['widget']['settings']['field_public_description']) . '</div>'
      ));
    }

  }
}